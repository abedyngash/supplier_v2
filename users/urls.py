from django.urls import path, include
from .views import home, SupplierOnboarding, profile

urlpatterns = [
	path('', home, name='home'),
	path('users/api/', include('users.api.v1.urls')),
	path('onboarding/', SupplierOnboarding.as_view(), name='onboarding'),
	path('profile/', profile, name='profile')
]
