from django.shortcuts import redirect
from django import forms
from django.contrib import messages
from django.contrib.auth import login, get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from allauth.account.forms import LoginForm, SignupForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field, Div, HTML, ButtonHolder
from crispy_forms.bootstrap import AppendedText, PrependedText

from .models import Company, Owner, Documentation

User = get_user_model()

''' LOGIN FORM '''
class CustomLoginForm(LoginForm):
	"""docstring for LoginForm"""
	def __init__(self, *args, **kwargs):
		super(CustomLoginForm, self).__init__(*args, **kwargs)

		self.helper = FormHelper()
		self.helper.form_tag = False
		self.fields["login"].label = ""
		self.fields["password"].label = ""

		self.helper.layout = Layout(
			Field('login', placeholder="Email Address"),
			AppendedText('password', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>', placeholder="Enter Password"),

			Field('remember'),
		)

''' USER REGISTRATION FORM '''
class RegistrationForm(UserCreationForm):
	class Meta:
		model = User
		fields = [
			'first_name', 'last_name', 'email',
			'password1', 'password2'
		]

		labels = {
			'first_name': 'First Name',
			'last_name': 'Last Name',
		}

	def signup(self, request, user):
		login(request, user)
		return redirect('home')

	def save(self, commit=False):
		user = super(RegistrationForm, self).save()
		return user

	def __init__(self, *args, **kwargs):
	    super(RegistrationForm, self).__init__(*args, **kwargs)
	    self.fields['first_name'].required=True
	    self.fields['last_name'].required=True

	    self.fields['password2'].label = 'Confirm Password'

	    self.fields['password1'].help_text=''
	    self.fields['password2'].help_text=''

	    self.helper = FormHelper()
	    self.helper.form_tag = False

	    self.helper.layout = Layout(	    	
			Row(
				Column(
					Field('first_name', css_id="", css_class=""),
    			),
				Column(
					Field('last_name', css_id="", css_class=""),
    			),
	    	),
	    	
	    	Row(
    			Column(
					Field('email', css_id="", css_class=""),
    			)
    		),	
    		Row(
    			Column(
					AppendedText('password1', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>'),
    			),
    			Column(
					AppendedText('password2', '<i class="fas fa-eye-slash" id="eye2" onclick="showHidePwd();"></i>'),
    			)
    		),
	    )

'''COMPANY GENERAL INFO'''
class CompanyForm(forms.ModelForm):
	class Meta:
		model = Company
		exclude = ['preferred_bank', 'preferred_bank_location']

	def __init__(self, *args, **kwargs):
	    super(CompanyForm, self).__init__(*args, **kwargs)

	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
	    		Column(Field('name')),
	    	),
	    	Row(
	    		Column(Field('company_email')),
	    		Column(Field('company_phone')),
	    	),
	    	Row(
	    		Column(Field('nature_of_business')),
	    		Column(Field('year_of_registration')),
	    	),
	    	Row(
	    		Column(Field('contact_person')),
	    		Column(Field('contact_position')),

	    	),
	    	Row(
	    		Column(Field('contact_phone')),
	    		Column(Field('contact_email')),
	    	),
	    	ButtonHolder(
		    	HTML(
		    		'\
		    		{% if "onboarding" in request.get_full_path %}\
		    			{% include "users/onboarding/_buttons.html" %}\
		    			{% else %}\
		    			<button type="submit" class="btn btn-danger">Submit</button>\
		    		{% endif %}\
		    		'
		    	)
	    	)
	    )

class BankForm(forms.ModelForm):
	class Meta:
		model = Company
		fields = ['preferred_bank', 'preferred_bank_location']

	def __init__(self, *args, **kwargs):
	    super(BankForm, self).__init__(*args, **kwargs)

	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
	    		Column(Field('preferred_bank')),
	    		Column(Field('preferred_bank_location')),
	    	),
	    	ButtonHolder(
		    	HTML(
		    		'\
		    		{% if "onboarding" in request.get_full_path %}\
		    			{% include "users/onboarding/_buttons.html" %}\
		    			{% else %}\
		    			<button type="submit" class="btn btn-danger">Submit</button>\
		    		{% endif %}\
		    		'
		    	)
	    	)
	    )

class OwnershipForm(forms.ModelForm):
	class Meta:
		model = Owner
		exclude = ['company']

class DocumentationForm(forms.ModelForm):
	class Meta:
		model = Documentation
		exclude = ['company']

		widgets = {
			'description': forms.Textarea(attrs={'style': 'height: 31px;'})
		}

OwnershipFormSet = forms.formset_factory(OwnershipForm, extra=1)
DocumentationFormSet = forms.formset_factory(DocumentationForm, extra=1)
