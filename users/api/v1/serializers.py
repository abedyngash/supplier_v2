from rest_framework import serializers

from users.models import Company, Anchor

class SupplierViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Company
		fields = [
			'id',
			'name',
			'company_email', 'company_phone',
			'nature_of_business', 'year_of_registration',
			'contact_person', 'contact_phone',
			'contact_position', 'contact_email',
			'preferred_bank', 'preferred_bank_location',
			'owners', 'documentations', 'users',
		]
		depth = 1

class AnchorViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Anchor
		fields = [
			'id', 'name',
			'contact_person', 'contact_phone', 'contact_email'
		]