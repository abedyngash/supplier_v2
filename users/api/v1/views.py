from rest_framework import viewsets
from users.models import Company, Anchor

from .serializers import SupplierViewsetSerializer, AnchorViewsetSerializer

class SupplierViewSet(viewsets.ModelViewSet):
	queryset = Company.objects.all().order_by('id')
	serializer_class = SupplierViewsetSerializer

class AnchorViewSet(viewsets.ModelViewSet):
	queryset = Anchor.objects.all().order_by('id')
	serializer_class = AnchorViewsetSerializer
