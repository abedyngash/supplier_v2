from django.urls import path, include
from rest_framework import routers

from .views import SupplierViewSet, AnchorViewSet

router = routers.DefaultRouter()
router.register(r'suppliers', SupplierViewSet)
router.register(r'anchors', AnchorViewSet)

urlpatterns = [
	path('', include(router.urls))
]