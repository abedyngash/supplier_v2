from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Company(models.Model):
	'''
	Reference to the DB Design
	'''
	BUSINESS_TYPES = (
		('Agriculture', 'Agriculture'),
		('Metal Production', 'Metal Production'),
		('Chemical Industries', 'Chemical Industries'),
		('Commerce', 'Commerce'),
		('Construction', 'Construction'),
		('Education', 'Education'),
		('Financial/Professional Services', 'Financial/Professional Services'),
		('Eateries', 'Eateries'),
		('Forestry', 'Forestry'),
		('Health Services', 'Health Services'),
		('Tourism/Hospitality', 'Tourism/Hospitality'),
		('Mining', 'Mining'),
		('Mechanical/Electrical Engineering', 'Mechanical/Electrical Engineering'),
		('Media/Culture', 'Media/Culture'),
		('Oil/Gas', 'Oil/Gas'),
		('Postal/Telecommunication', 'Postal/Telecommunication'),
		('Public Service', 'Public Service'),
		('Wateries', 'Wateries'),
		('Textiles', 'Textiles'),
		('Transport', 'Transport'),
		('Utilities', 'Utilities')
	)

	BANKS_LIST = (
	    	('ABC Bank (Kenya)', 'ABC Bank (Kenya)'),
		    ('Absa Bank Kenya', 'Absa Bank Kenya'),
		    ('Access Bank Kenya', 'Access Bank Kenya'),
		    ('Bank of Africa', 'Bank of Africa'),
		    ('Bank of Baroda', 'Bank of Baroda'),
		    ('Bank of India', 'Bank of India'),
		    ('Citibank', 'Citibank'),
		    ('Consolidated Bank of Kenya', 'Consolidated Bank of Kenya'),
		    ('Cooperative Bank of Kenya', 'Cooperative Bank of Kenya'),
		    ('Credit Bank', 'Credit Bank'),
		    ('Development Bank of Kenya', 'Development Bank of Kenya'),
		    ('Diamond Trust Bank', 'Diamond Trust Bank'),
		    ('Dubai Islamic Bank', 'Dubai Islamic Bank'),
		    ('Ecobank Kenya', 'Ecobank Kenya'),
		    ('Equity Bank Kenya', 'Equity Bank Kenya'),
		    ('Family Bank', 'Family Bank'),
		    ('First Community Bank', 'First Community Bank'),
		    ('Guaranty Trust Bank Kenya', 'Guaranty Trust Bank Kenya'),
		    ('Guardian Bank', 'Guardian Bank'),
		    ('Gulf African Bank', 'Gulf African Bank'),
		    ('Habib Bank AG Zurich', 'Habib Bank AG Zurich'),
		    ('Housing Finance Company of Kenya', 'Housing Finance Company of Kenya'),
		    ('I&M Bank', 'I&M Bank'),
		    ('Imperial Bank Kenya', 'Imperial Bank Kenya'),
		    ('Kingdom Bank Limited', 'Kingdom Bank Limited'),
		    ('Kenya Commercial Bank', 'Kenya Commercial Bank'),
		    ('Mayfair Bank', 'Mayfair Bank'),
		    ('Middle East Bank Kenya', 'Middle East Bank Kenya'),
		    ('M Oriental Bank', 'M Oriental Bank'),
		    ('National Bank of Kenya', 'National Bank of Kenya'),
		    ('NCBA Bank Kenya', 'NCBA Bank Kenya'),
		    ('Paramount Universal Bank', 'Paramount Universal Bank'),
		    ('Prime Bank (Kenya)', 'Prime Bank (Kenya)'),
		    ('SBM Bank Kenya', 'SBM Bank Kenya'),
		    ('Sidian Bank', 'Sidian Bank'),
		    ('Spire Bank', 'Spire Bank'),
		    ('Stanbic Holdings Plc', 'Stanbic Holdings Plc'),
		    ('Standard Chartered Kenya', 'Standard Chartered Kenya'),
		    ('United Bank for Africa', 'United Bank for Africa'),
		    ('Victoria Commercial Bank', 'Victoria Commercial Bank'),
		)

	name = models.CharField(max_length=255)
	company_email = models.EmailField("Company Email Address")
	company_phone = models.CharField('Company Phone Number', max_length=13)
	nature_of_business = models.CharField('Nature of Business', max_length=100, choices=BUSINESS_TYPES)
	year_of_registration = models.IntegerField('Year of Registration')
	contact_person = models.CharField('Name of Contact Person', max_length=100, null=True)
	contact_phone = models.CharField('Contact Person Phone Number', max_length=13, null=True)
	contact_position = models.CharField('Contact Person Position', max_length=13, null=True)
	contact_email = models.EmailField('Contact Person Email Address', null=True)
	preferred_bank = models.CharField('Preferred Bank', max_length=100, choices=BANKS_LIST)
	preferred_bank_location = models.CharField('Preferred Bank Location', max_length=100)

	class Meta:
		verbose_name_plural = 'companies'

	def __str__(self):
		return f'{self.name}'

class User(AbstractUser):	
	USER_TYPES = (
		(1, 'Financier'),
		(2, 'Supplier'),
		(3, 'Anchor'),
		(4, 'Customer Analyst')
	)
	company = models.ForeignKey('users.Company', related_name='users', on_delete=models.CASCADE, null=True)
	username = None
	user_type = models.IntegerField(choices=USER_TYPES, default=2)
	email = models.EmailField(unique=True)
	last_logout = models.DateTimeField(blank=True, null=True)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	def __str__(self):
		return f'{self.first_name} {self.last_name}'

class Owner(models.Model):
	company = models.ForeignKey('users.Company', related_name='owners', on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	email = models.EmailField()
	phone = models.CharField(max_length=13)
	address = models.CharField(max_length=255)

	def __str__(self):
		return f'{self.name}'

class Documentation(models.Model):
	
	DOCUMENT_TYPES = (
		('kra_pin', 'KRA PIN Certificate'),
		('cr_12', 'CR 12'),
		('business_registration', 'Business Registration Certificate')
	)
	company = models.ForeignKey(Company, related_name='documentations', on_delete=models.CASCADE)
	document_type = models.CharField(choices=DOCUMENT_TYPES, max_length=30)
	description = models.TextField(max_length=255)
	file = models.FileField(upload_to='company_documents/')

	def __str__(self):
		return f'{self.document_type}'

class Anchor(models.Model):
	name = models.CharField('name', max_length=255)
	contact_person = models.CharField('Name of Contact Person', max_length=13)
	contact_phone = models.CharField('Contact Person Phone', max_length=13)
	contact_email = models.EmailField('contact Person Email')

	def __str__(self):
		return f'{self.name}'

class Financier(models.Model):	
	name = models.CharField(max_length=255)
	company_email = models.EmailField('Company Email', unique=True)
	company_phone = models.CharField('Company Phone', max_length=13)
	contact_person = models.CharField('Name of Contact Person', max_length=100)
	contact_phone = models.CharField('Contact Person Phone Number', max_length=100)
	contact_email = models.EmailField('Contact Person Email')

	def __str__(self):
		return f'{self.name}'