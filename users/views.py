import os
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import ListView
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage

from formtools.wizard.views import SessionWizardView

from dealbook.forms import AskUserForActiveDeals, AskUserForConcludedDeals, ActiveDealForm, ConcludedDealForm
from dealbook.utils import active_deals_form_condition, concluded_deals_form_condition

from .models import Company, Owner, Documentation
from .forms import CompanyForm, BankForm, OwnershipFormSet, DocumentationFormSet
from .decorators import company_required
from .utils import create_wizard_formset_objects, create_wizard_form_objects
# Create your views here.

'''
# Allauth
-> Login/Logout/Signup/Password Management
-> Create anchor/customer from Deal data
'''
@login_required
@company_required
def home(request):
	return redirect('profile')

'''
Handle the whole onboarding process
'''
class SupplierOnboarding(LoginRequiredMixin, SessionWizardView):
	'''
	Steps
	1. Basic Info
	2. Owners
	3. Bank
	4. Documents
	5. Dealbook

	'''
	form_list = [
		('company_info', CompanyForm),
		('ownership', OwnershipFormSet),
		('bank', BankForm),
		('documentations', DocumentationFormSet),
		('check_for_concluded_deals', AskUserForConcludedDeals),
		('concluded_deals', ConcludedDealForm),
		('check_for_active_deals', AskUserForActiveDeals),
		('active_deals', ActiveDealForm),
	]

	TEMPLATES = {
		'company_info': 'users/onboarding/company_info.html',
		'ownership': 'users/onboarding/ownership.html',
		'bank': 'users/onboarding/bank.html',
		'documentations': 'users/onboarding/documentations.html',
		'check_for_concluded_deals': 'users/onboarding/ownership.html',
		'concluded_deals': 'users/onboarding/company_info.html',
		'check_for_active_deals': 'users/onboarding/ownership.html',
		'active_deals': 'users/onboarding/ownership.html'
	}

	file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'supplier_onboarding_files'))

	condition_dict = {
		'active_deals': active_deals_form_condition,
		'concluded_deals': concluded_deals_form_condition
	}

	def get_template_names(self):
		return [self.TEMPLATES[self.steps.current]]

	def done(self, form_list, **kwargs):
		'''
		-> Get all Company related data, then save
		-> Save owners
		-> Save documentations
		-> Save deals
		'''
		company_cleaned_data = {
			**self.get_cleaned_data_for_step('company_info'),
			**self.get_cleaned_data_for_step('bank')
		}
		ownership_cleaned_data = self.get_cleaned_data_for_step('ownership')
		documentations_cleaned_data = self.get_cleaned_data_for_step('documentations')

		company = create_wizard_form_objects(company_cleaned_data, Company)
		self.request.user.company = company
		self.request.user.save()
		
		create_wizard_formset_objects(ownership_cleaned_data, Owner, company=company)
		create_wizard_formset_objects(documentations_cleaned_data, Documentation, company=company)


		next_url = self.request.GET.get('next') or reverse('home')
		return redirect(next_url)

'''
Deals with View/Updating/Creating

request.method == POST/GET
'''
def profile(request):
	'''
	Include:
		-> General Info
		-> Owners
		-> Documents

	'''
	company_form = CompanyForm(request.POST or None, instance=request.user.company)
	owners_form = OwnershipFormSet(request.POST or None)
	documents_form = DocumentationFormSet(request.POST or None)

	try:
		supplier_owners = request.user.company.owners.all()
		supplier_documents = request.user.company.documentations.all()
		pass
	except Exception:
		supplier_owners = None
		supplier_documents = None

	context = {
		'company_form': company_form,
		'owners_form': owners_form,
		'documents_form': documents_form,

		'supplier_owners': supplier_owners,
		'supplier_documents': supplier_documents,
	}

	tab = request.GET.get('tab')

	if tab == 'company-info':
		return render(request, 'users/profile/company_info.html', context)
	elif tab == 'ownership':
		return render(request, 'users/profile/ownership.html', context)
	elif tab == 'documentations':
		return render(request, 'users/profile/documentations.html', context)

	return render(request, 'users/profile/company_info.html', context)

'''
Add other members of your company(With the ability to sign in)
'''
def add_company_peers(request):
	'''

	'''
	pass