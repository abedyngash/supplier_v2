def create_wizard_formset_objects(_list, _model, **kwargs):
	if _list:
		non_empty_list = [i for i in _list if bool(i)]
		for i in non_empty_list:
			# objects_to_pop = [str('DELETE'), 'user']
			final_dict = dict([(k, v) for k,v in i.items()])
			obj = _model.objects.create(**final_dict, **kwargs)

def create_wizard_form_objects(_dict, _model, **kwargs):
	obj = _model.objects.create(**_dict, **kwargs)
	return obj