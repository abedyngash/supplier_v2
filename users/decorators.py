from django.shortcuts import redirect
from django.urls import resolve, reverse

def company_required(function):
	"""
	Verifies whether the user sending this request is associated with a company registered in the system.

	If the particular user is not assoiciated with any company, the function returns a 403 Forbidden error.
	"""
	def wrap(request, *args, **kwargs):
		current_url = resolve(request.path_info).url_name
		current_url = reverse(f'{current_url}')	
		if not request.user.company:
			resp = f"{reverse('onboarding')}?next={current_url}"
			return redirect(resp)
		return function(request, *args, **kwargs)
	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	return wrap
