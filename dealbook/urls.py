from django.urls import path, include
from .views import ActiveDealListView, ConcludedDealListView, RequestsListView
from .views import ActiveDealCreateView, ConcludedDealCreateView, RequestCreateView
from .views import DealUpdateView, RequestUpdateView, DealDeleteView, RequestDeleteView, ConcludedDealDeleteView
from .views import UploadDealExcel, ApproveDealView

urlpatterns = [
	path('api/', include('dealbook.api.v1.urls')),
	path('active-deals/list/', ActiveDealListView.as_view(), name='active-deals-list'),
	path('active-deals/create/', ActiveDealCreateView.as_view(), name='active-deals-create'),
	path('deals/update/<int:pk>/', DealUpdateView.as_view(), name='deal-update'),
	path('deals/approve/<int:pk>/', ApproveDealView.as_view(), name='deal-approve'),
	path('concluded-deals/create/', ConcludedDealCreateView.as_view(), name='concluded-deals-create'),
	path('concluded-deals/list/', ConcludedDealListView.as_view(), name='concluded-deals-list'),
	path('financial-requests/create/', RequestCreateView.as_view(), name='requests-create'),
	path('financial-requests/list/', RequestsListView.as_view(), name='requests-list'),
	path('financial-requests/update/<int:pk>/', RequestUpdateView.as_view(), name='requests-update'),
	path('financial-requests/delete/<int:pk>/', RequestDeleteView.as_view(), name='request-delete'),
	path('deal/delete/<int:pk>/', DealDeleteView.as_view(), name='deal-delete'),
	path('concluded-deal/delete/<int:pk>/', ConcludedDealDeleteView.as_view(), name='concluded-deal-delete'),
	path('upload/', UploadDealExcel.as_view(), name='upload') # used to test uploading Excel files. For now it simply prints the data in the Excel file

]