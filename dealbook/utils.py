import openpyxl
from .models import Deal
from users.models import Company

def active_deals_form_condition(wizard):
	cleaned_data = wizard.get_cleaned_data_for_step('check_for_active_deals') or {}
	return cleaned_data.get('user_response', False)

def concluded_deals_form_condition(wizard):
	cleaned_data = wizard.get_cleaned_data_for_step('check_for_concluded_deals') or {}
	return cleaned_data.get('user_response', False)


def upload_excel_file_deals(the_file, user):
	"""
	Handles uploaded Excel file containing deal information. The 
	file should contain a header row with the following values:
	Customer, Customer contact person, Customer contact email,
	Customer contact phone, Customer contact position, 
	LPO number, Product, Quantity, Unit value, Gross order value,
	Date of issue, Expected delivery date, Actual delivery date, 
	Date of invoice, Date of payment.

	Subsequent rows should contain the deals-data. The following values
	are optional: Actual delivery date, Date of Invoice, Date of payment.
	If the actual delivery date is absent, the deal is assumed to be an 
	active deal, otherwise the deal is a concluded deal.

	The data types of the values in the cells can be deduced from the 
	column names. In the event of an invalid data type, an Exception may 
	be thrown.

	:type the_file: 
	:type user: the currently logged in user

	"""
	supplier = user # syntactic sugar 
	f = openpyxl.load_workbook(the_file)
	sheet = f.worksheets[0]
	rows = sheet.rows
	# The first row is skipped as it is the header row
	row_data = []
	for row in rows:
		row_data.append(row)
	for r in range(1, len(row_data)):		
		
		# we cannot use bulk_create since Deal has relationships with other models
		# also, we are relying on post_save signal which will not be fired when using bulk_create
		# print('Customer: ', row_data[r][0].value)
		deal = Deal(customer=row_data[r][0].value, customer_contact_person=row_data[r][1].value, 
			customer_contact_email=row_data[r][2].value, customer_contact_phone=row_data[r][3].value, 
			customer_contact_position=row_data[r][4].value, lpo_number=row_data[r][5].value, product=row_data[r][6].value, 
			quantity=row_data[r][7].value, unit_value=row_data[r][8].value, gross_order_value=row_data[r][9].value, 
			date_of_issue=row_data[r][10].value, expected_delivery_date=row_data[r][11].value, 
			actual_delivery_date=row_data[r][12].value, date_of_invoice=row_data[r][13].value, 
			date_of_payment=row_data[r][14].value) # by default, deal is not approved
		supplier.deals.add(deal, bulk=False)
	f.close()
	print('Excel file deals saved.')