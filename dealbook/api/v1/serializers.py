from rest_framework import serializers

from dealbook.models import Deal, Request, Product
from users.models import Company, Anchor

class DealViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Deal
		fields = [
			'id',
			'supplier',
			'customer', 'customer_contact_person', 'customer_contact_email',
			'customer_contact_phone', 'customer_contact_position',
			'lpo_number', 'quantity',
			'unit_value', 'gross_order_value',
			'date_of_issue', 'expected_delivery_date', 'actual_delivery_date',
			'date_of_invoice', 'date_of_payment',
			'document',
		]
		depth = 1

class RequestViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Request
		fields = [
			'id',
			'supplier', 'deal', 'financier',
			'total_amount_requested', 'expected_disbursement_date',
			'bank_name', 'account_name', 'account_number'
		]
		depth = 1

class ProductViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		fields = [
			'name', 
			'suppliers', 'buyers'
		]

class SupplierViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Company
		fields = [
			'name',
			'company_email',
			'company_phone',
			'nature_of_business',
			'year_of_registration',
			'contact_person',
			'contact_phone',
			'contact_position',
			'contact_email',
			'preferred_bank',
			'preferred_bank_location'
		]
		depth = 1

class SupplierRequestViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Request
		fields = [
			'id',
			'deal',
			'financier',
			'cost_of_goods',
			'cost_of_logistics',
			'total_amount_requested',
			'expected_disbursement_date',
			'bank_name',
			'account_number',
			'account_name'
		]
		depth = 1

class SupplierProductViewsetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		fields = ['name']

class AnchorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Anchor
		fields = [
			'name', 'contact_person', 'contact_phone', 'contact_email'
		]