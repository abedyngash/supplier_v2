from rest_framework import viewsets
from dealbook.models import Deal, Request, Product
from users.models import Company, Anchor
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import DealViewsetSerializer, RequestViewsetSerializer, ProductViewsetSerializer
from .serializers import SupplierViewsetSerializer, SupplierRequestViewsetSerializer, SupplierProductViewsetSerializer
from .serializers import AnchorSerializer

class DealViewSet(viewsets.ModelViewSet):
	queryset = Deal.objects.all().order_by('id')
	serializer_class = DealViewsetSerializer
	http_method_names = ['get']

class RequestViewSet(viewsets.ModelViewSet):
	queryset = Request.objects.all().order_by('id')
	serializer_class = RequestViewsetSerializer
	http_method_names = ['get', 'put']

class ProductViewSet(viewsets.ModelViewSet):
	queryset = Product.objects.all().order_by('id')
	serializer_class = ProductViewsetSerializer
	http_method_names = ['get']

class SupplierViewSet(viewsets.ModelViewSet):
	queryset = Company.objects.all().order_by('id')
	serializer_class = SupplierViewsetSerializer
	http_method_names = ['get']

class SupplierRequests(APIView):
	def get(self, request, format=None, *args, **kwargs):
		supplier = kwargs.get('supplier')
		# add code to filter requests that the supplier has consented to, 
		# show requests that have status=...
		requests = Request.objects.filter(supplier=int(supplier)).order_by('id')
		serializer = SupplierRequestViewsetSerializer(requests, many=True)
		return Response(serializer.data)

class SupplierDeals(APIView):
	def get(self, request, format=None, *args, **kwargs):
		supplier = kwargs.get('supplier')
		deals = Deal.objects.filter(supplier=int(supplier)).order_by('id')
		serializer = DealViewsetSerializer(deals, many=True)
		return Response(serializer.data)

class SupplierProducts(APIView):
	def get(self, request, format=None, *args, **kwargs):
		supplier = kwargs.get('supplier')
		# distinct() is needed in case supplier supplied same product to different buyers
		products = Product.objects.filter(suppliers=int(supplier)).values('name').distinct().order_by('name')
		serializer = SupplierProductViewsetSerializer(products, many=True)
		return Response(serializer.data)

class SupplierAnchors(APIView):
	def get(self, request, format=None, *args, **kwargs):
		supplier = kwargs.get('supplier')
		# Anchor model did not create a relationship between an anchor and a supplier
		# Also, Deal model did not create a relationship between the deal and an Anchor
		# However, the Deal model includes customer and customer_contact_email fields which are the same 
		# as the contact_person and contact_email fields in Anchor.
		# So to get a supplier's Anchors, we go through Deals.
		deals = Deal.objects.filter(supplier=int(supplier))
		emails = [deal.customer_contact_email for deal in deals]
		anchors = Anchor.objects.filter(contact_email__in=emails)
		serailizer = AnchorSerializer(anchors, many=True)
		return Response(serailizer.data)
