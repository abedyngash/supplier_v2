from django.urls import path, include
from rest_framework import routers
from .views import DealViewSet, RequestViewSet, ProductViewSet
from .views import SupplierDeals
from .views import SupplierViewSet, SupplierRequests, SupplierProducts, SupplierAnchors

router = routers.DefaultRouter()
router.register(r'deals', DealViewSet)
router.register(r'requests', RequestViewSet)
router.register(r'products', ProductViewSet)

urlpatterns = [
	path('', include(router.urls)),
	path('deals/supplier/<int:supplier>', SupplierDeals.as_view(), name='rest-supplier-deals' ),
	path('requests/supplier/<int:supplier>', SupplierRequests.as_view(), name='rest-supplier-requests' ),
	path('products/supplier/<int:supplier>', SupplierProducts.as_view(), name='rest-supplier-products'),
	path('anchors/supplier/<int:supplier>', SupplierAnchors.as_view(), name='rest-supplier-anchors')
]