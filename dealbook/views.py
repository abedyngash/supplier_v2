from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.db.models import Q
from .models import Deal, Request
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from .forms import ActiveDealForm, ConcludedDealForm, RequestForm, DealForm, UploadExcelTest, ApproveDealForm
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from bootstrap_modal_forms.generic import BSModalUpdateView, BSModalCreateView, BSModalDeleteView, BSModalFormView
from .utils import upload_excel_file_deals

import openpyxl

class ActiveDealListView(LoginRequiredMixin, ListView):
	template_name = 'dealbook/lists/active_deals_list.html'
	def get_queryset(self):
		# filter deals for the current supplier
		# filter deals that are active (not concluded)
		# return Deal.objects.all()
		return Deal.objects.filter(supplier=self.request.user.company, actual_delivery_date=None) 
		
class ConcludedDealListView(LoginRequiredMixin, ListView):
	template_name = 'dealbook/lists/concluded_deals_list.html'
	def get_queryset(self):
		# filter deals for the current supplier
		# filter deals that are not active (concluded)
		return Deal.objects.filter(supplier=self.request.user.company, 
			actual_delivery_date__isnull=False)

class RequestsListView(LoginRequiredMixin, ListView):
	template_name = 'dealbook/lists/requests_list.html'
	def get_queryset(self):
		return Request.objects.filter(supplier=self.request.user.company)

class ActiveDealCreateView(LoginRequiredMixin,BSModalCreateView):
	model = Deal
	form_class = ActiveDealForm
	success_url = reverse_lazy('active-deals-list')
	template_name = 'dealbook/create/active_deal_create.html'

	def form_valid(self, form):
		super(ActiveDealCreateView, self).form_valid(form)
		messages.success(self.request, 'Active deal saved successfully.')
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		"""
		POST overriden so that we can set the supplier field to 
		the currently logged in supplier. Perhaps there is a better
		way of achieving this??
		"""
		form = self.get_form()
		if form.is_valid():
			# set the supplier
			form.instance.supplier = request.user.company
			super(ActiveDealCreateView, self).form_valid(form)
			messages.success(request, 'Active deal saved successfully.')
			return HttpResponseRedirect(self.get_success_url())
		self.object = None
		return self.form_invalid(form)

class ConcludedDealCreateView(LoginRequiredMixin, BSModalCreateView):
	model = Deal
	form_class = ConcludedDealForm
	success_url = reverse_lazy('concluded-deals-list')
	template_name = 'dealbook/create/concluded_deal_create.html'

	def post(self, request, *args, **kwargs):
		"""
		POST overriden so that we can set the supplier field to 
		the currently logged in supplier. Perhaps there is a better
		way of achieving this??
		"""
		# TODO:Need to add some validation to check things like dates, e.g. the 
		# delivery date should not be before the issue date e.t.c.
		form = self.get_form()
		if form.is_valid():
			# set the supplier
			form.instance.supplier = request.user.company
			super(ConcludedDealCreateView, self).form_valid(form)
			messages.success(request, 'Concluded deal saved successfully.')
			return HttpResponseRedirect(self.get_success_url())
		self.object = None
		return self.form_invalid(form)

class RequestCreateView(LoginRequiredMixin, BSModalCreateView):
	model = Request
	form_class = RequestForm
	success_url = reverse_lazy('requests-list')
	template_name = 'dealbook/create/request_create.html'

	# get used to retrieve deals belonging to the current supplier
	# needs to show only deals that are active?
	# needs to show only deals that do not have requests yet?

	def get(self, request, *args, **kwargs):
		
		form = RequestForm()
		# show deals belonging to the currenly logged in supplier
		form.fields['deal'].queryset = Deal.objects.filter(supplier=request.user.company, 
			actual_delivery_date__isnull=True)
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		"""
		POST overriden so that we can set the supplier field to 
		the currently logged in supplier. Perhaps there is a better
		way of achieving this??
		"""
		form = self.get_form()
		if form.is_valid():
			# set the supplier
			form.instance.supplier = request.user.company
			super(RequestCreateView, self).form_valid(form)
			messages.success(request, 'Request created saved successfully.')
			return HttpResponseRedirect(self.get_success_url())
		self.object = None
		return self.form_invalid(form)

class DealUpdateView(LoginRequiredMixin, BSModalUpdateView):
	model = Deal
	form_class = DealForm
	success_url = reverse_lazy('active-deals-list')
	template_name = 'dealbook/update/deal_update.html'
	success_message = 'Deal updated successfully.'

class RequestUpdateView(LoginRequiredMixin, BSModalUpdateView):
	model = Request
	form_class = RequestForm
	success_url = reverse_lazy('requests-list')
	template_name = 'dealbook/update/request_update.html'
	success_message = 'Request updated successfully'

class DealDeleteView(LoginRequiredMixin, BSModalDeleteView):
	model = Deal
	success_url = reverse_lazy('active-deals-list')
	template_name = 'dealbook/delete/deal_delete.html'
	success_message = 'Deal deleted successfully'

class ConcludedDealDeleteView(LoginRequiredMixin, BSModalDeleteView):
	model = Deal
	success_url = reverse_lazy('concluded-deals-list')
	template_name = 'dealbook/delete/deal_delete.html'
	success_message = 'Deal deleted successfully'

class RequestDeleteView(LoginRequiredMixin, BSModalDeleteView):
	model = Request
	success_url = reverse_lazy('requests-list')
	template_name = 'dealbook/delete/request_delete.html'
	success_message = 'Request deleted successfully'

class UploadDealExcel(LoginRequiredMixin, BSModalFormView):
	success_url = reverse_lazy('active-deals-list')
	template_name = 'dealbook/upload/upload_excel.html'
	success_message = 'File uploaded successfully'

	def get(self, request, *args, **kwargs):
		form = UploadExcelTest()
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		form = UploadExcelTest(request.POST, request.FILES)
		if form.is_valid():
			for field in request.FILES.keys():
				for formfile in request.FILES.getlist(field):
					try:
						upload_excel_file_deals(formfile, request.user.company)
					except:
						# show error message
						print('An error occurred')
						return HttpResponseRedirect(reverse_lazy('active-deals-list'))						

			return HttpResponseRedirect(reverse_lazy('active-deals-list'))
		else:
			# show error message
			return HttpResponseRedirect(reverse_lazy('active-deals-list'))

class ApproveDealView(LoginRequiredMixin, BSModalUpdateView):
	model = Deal
	form_class = ApproveDealForm
	success_url = reverse_lazy('active-deals-list')
	success_message = 'Deal approved'
	template_name = 'dealbook/update/approve_deal.html'

	def form_valid(self, form):
		form.instance.is_approved = True
		form.instance.save()
		messages.success(self.request, 'Deal approved.')
		return HttpResponseRedirect(reverse_lazy('active-deals-list'))

def verify_deal(request, deal_id):
	"""
	Send email to buyer to verify deal
	"""
	
	pass