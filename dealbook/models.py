from django.db import models

# Create your models here.
class Deal(models.Model):
	supplier = models.ForeignKey('users.Company', related_name='deals', on_delete=models.CASCADE, null=True, blank=True)
	customer = models.CharField(max_length=255, null=True, blank=True)
	customer_contact_person = models.CharField("Name of Customer Contact Person", max_length=255, null=True, blank=True)
	customer_contact_email = models.EmailField("Email Address of Customer Contact Person", null=True, blank=True)
	customer_contact_phone = models.CharField("Phone Number of Customer Contact Person", max_length=255, null=True, blank=True)
	customer_contact_position = models.CharField("Position of Customer Contact Person", max_length=255, null=True, blank=True)
	lpo_number = models.CharField("LPO Number", max_length=255, null=True, blank=True)
	product = models.CharField(max_length=255, null=True, blank=True)
	quantity = models.IntegerField(null=True, blank=True)
	unit_value = models.IntegerField(null=True, blank=True)
	gross_order_value = models.PositiveIntegerField(null=True, blank=True)
	date_of_issue = models.DateField(null=True, blank=True)
	expected_delivery_date = models.DateField(null=True, blank=True)
	actual_delivery_date = models.DateField(null=True, blank=True)
	date_of_invoice = models.DateField(null=True, blank=True)
	date_of_payment = models.DateField(null=True, blank=True)
	is_approved = models.BooleanField(default=False) # not approved by default
	is_verified = models.BooleanField(default=False) # not verified by default
	document = models.FileField(upload_to='deal_documents/', null=True, blank=True)

	def __str__(self):
		return f'{self.supplier} - {self.product}'


class Request(models.Model):
	
	REQUEST_STATUS = (
		(1, 'unattended'),
		(2, 'lead'),
		(3, 'proposals made'),
		(4, 'negotiations'),
		(5, 'deal')
	)

	supplier = models.ForeignKey('users.Company', related_name='requests', on_delete=models.CASCADE, null=True, blank=True)
	deal = models.ForeignKey(Deal, related_name='requests', on_delete=models.CASCADE)
	financier = models.ForeignKey('users.Financier', related_name='requests', on_delete=models.CASCADE, null=True, blank=True)
	cost_of_goods = models.BigIntegerField(null=True, blank=True)
	cost_of_logistics = models.BigIntegerField(null=True, blank=True)
	total_amount_requested = models.BigIntegerField()
	expected_disbursement_date = models.DateField()
	bank_name = models.CharField(max_length=255)
	account_number = models.CharField(max_length=255)
	account_name = models.CharField(max_length=255)
	# default status for choice to be unattended??
	status = models.IntegerField(choices=REQUEST_STATUS, default=1)
	consent = models.BooleanField(default=False)

	def __str__(self):
		return f'{self.supplier} - {self.deal}'
	

class Product(models.Model):
	name = models.CharField(max_length=255)
	suppliers = models.ManyToManyField('users.Company')
	buyers = models.ManyToManyField('users.Anchor')

	def __str__(self):
		return f'{self.name}'
	