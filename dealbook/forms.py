from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field, Div, HTML, ButtonHolder
from .models import Deal, Request
from bootstrap_modal_forms.forms import BSModalModelForm, BSModalForm


BOOLEAN_YN = (
	(True, u'Yes'),
	(False, u'No'),  
)

# Class below does not distinguish between active and 
# concluded deals
class DealForm(BSModalModelForm):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.layout = Layout(
			Row(Column(Field('customer')),
			),
			Row(Column(Field('customer_contact_person')),
				Column(Field('customer_contact_email')),
				Column(Field('customer_contact_phone')),
				Column(Field('customer_contact_position')),
			),
			Row(
				Column(Field('lpo_number')),
			),
			Row(
				Column(Field('product')),
				Column(Field('quantity')),
			),
			Row(
				Column(Field('unit_value')),
				Column(Field('gross_order_value')),
			),
			Row(
				Column(Field('date_of_issue')),
				Column(Field('expected_delivery_date')),
				Column(Field('actual_delivery_date')),
			),
			Row(Column(Field('date_of_invoice')),
				Column(Field('date_of_payment')),
			)
		)
	class Meta:
		model = Deal
		fields = ['customer', 'customer_contact_person', 
					'customer_contact_email', 'customer_contact_phone',
					'customer_contact_position', 'lpo_number', 
					'product', 'quantity', 'unit_value', 'gross_order_value',
					'date_of_issue', 'expected_delivery_date'
					# excluded: supplier,
					# is_approved,
					# is_verified, document
				]

class ActiveDealForm(BSModalModelForm):
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.layout = Layout(
			Row(
				Column(Field('customer')),
			),
			Row(
				Column(Field('customer_contact_person')),
				Column(Field('customer_contact_email')),
				Column(Field('customer_contact_phone')),
				Column(Field('customer_contact_position'))
			),
			Row(
				Column(Field('lpo_number')),
			),
			Row(
				Column(Field('product')),
				Column(Field('quantity')),
			),
			Row(
				Column(Field('unit_value')),
				Column(Field('gross_order_value')),
			),
			Row(
				Column(Field('date_of_issue')),
				Column(Field('expected_delivery_date')),
			),
		)

	class Meta:
		model = Deal
		fields = ['customer', 'customer_contact_person', 
					'customer_contact_email', 'customer_contact_phone',
					'customer_contact_position', 'lpo_number', 
					'product', 'quantity', 'unit_value', 'gross_order_value',
					'date_of_issue', 'expected_delivery_date'
					# excluded: supplier, actual_delivery_date, 
					# date_of_invoice, date_of_payment, is_approved,
					# is_verified, document
					]

class ConcludedDealForm(BSModalModelForm):
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.layout = Layout(
			Row(
				Column(Field('customer')),
			),
			Row(
				Column(Field('customer_contact_person')),
				Column(Field('customer_contact_email')),
				Column(Field('customer_contact_phone')),
				Column(Field('customer_contact_position'))
			),
			Row(
				Column(Field('product')),
			),
			Row(
				Column(Field('quantity')),
				Column(Field('unit_value')),
				Column(Field('gross_order_value')),
			),
			Row(
				Column(Field('date_of_issue')),
				Column(Field('actual_delivery_date')),
			),
			Row(
				Column(Field('lpo_number')),
			),
		)
        
	class Meta:
		model = Deal
		fields = ['customer', 'customer_contact_person', 
					'customer_contact_email', 'customer_contact_phone',
					'customer_contact_position', 'lpo_number', 
					'product', 'quantity', 'unit_value', 'gross_order_value',
					'date_of_issue', 'actual_delivery_date',
					# excluded: supplier, expected_delivery_date, 
					# date_of_invoice, date_of_payment, is_approved,
					# is_verified, document
					]

class RequestForm(BSModalModelForm):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.layout = Layout(
			Row(
				Column(Field('deal')),
			),
			Row(
				Column(Field('cost_of_goods')),
			),
			Row(
				Column(Field('cost_of_logistics')),
			),
			Row(
				Column(Field('total_amount_requested')),
			),
			Row(
				Column(Field('expected_disbursement_date')),
			),
			Row(
				Column(Field('bank_name')),
				Column(Field('account_number')),
				Column(Field('account_name')),
			),
		)
	
	class Meta:
		model = Request
		fields = ['deal', 'cost_of_goods', 'cost_of_logistics', 
					'total_amount_requested', 'expected_disbursement_date',
					'bank_name', 'account_number', 'account_name']

class AskUserForActiveDeals(forms.Form):
	user_response = forms.TypedChoiceField(
                   coerce=lambda x: x == 'True',
                   choices=BOOLEAN_YN,
                   widget=forms.RadioSelect,
                   label='Active Deals?'
                )

class AskUserForConcludedDeals(forms.Form):
	user_response = forms.TypedChoiceField(
                   coerce=lambda x: x == 'True',
                   choices=BOOLEAN_YN,
                   widget=forms.RadioSelect,
                   label='Concluded Deals?'
                )

class ApproveDealForm(BSModalModelForm):
	class Meta:
		model = Deal
		fields = [] # no need to display any fields as we are only approving the deal


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xlsx', '.xls']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Only Excel files are allowed')

class UploadExcelTest(BSModalForm):
	file = forms.FileField(label='Excel File', validators=[validate_file_extension])