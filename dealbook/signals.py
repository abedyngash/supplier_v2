from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import Deal
from .models import Product
from users.models import Anchor

# @receiver(post_save, sender=Deal)
# def map_deal_record_to_anchor(self, instance, created, **kwargs):
# 	if created:
# 		anchor, added = Anchor.objects.get_or_create(name=instance.customer,
# 			contact_email=instance.customer_contact_email)

# signal to add product to database when deal is saved
# Since the method above(map_deal_record_to_anchor) is also called when
# a Deal is saved, perhaps the logic can be combined with the method below?
@receiver(post_save, sender=Deal)
def check_or_add_product(sender, instance, created, **kwargs):
	"""
	Checks if the product for the supplier and buyer already exists 
	in the products table. If the product exists, nothing happens, 
	otherwise it is added into the products table
	"""
	if created:

		# add the Anchor if the anchor does not exist
		customer = instance.customer
		supplier = instance.supplier
		customer_email = instance.customer_contact_email
		anchor, added = Anchor.objects.get_or_create(name=customer, 
			contact_email=customer_email)		
		# add the Product if it does not exist
		product_name = instance.product
		try:
			Product.objects.get(name=product_name, suppliers=supplier,
				buyers=anchor)
		except Product.DoesNotExist:
			# no matching product, so create the product
			p = Product.objects.create(name=product_name)
			p.save()
			p.suppliers.add(supplier)
			p.buyers.add(anchor)
			p.save()
